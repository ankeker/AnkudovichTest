package com.testtask.apptesttask.entity

data class AppConfig(val ts: String, val publicKey: String, val hash: String)