package com.testtask.apptesttask.presentation.profile

import com.arellomobile.mvp.InjectViewState
import com.testtask.apptesttask.presentation.global.BasePresenter

@InjectViewState
class ProfilePresenter : BasePresenter<ProfileView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        // TODO: profile tab (Add view implementation).
    }
}